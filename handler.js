'use strict';

var airtableAPI = require('./airtableApi')
var fs = require('fs')
require("dotenv").config();

function sleep(ms) {
	return new Promise((resolve) => {
		setTimeout(resolve, ms);
	});
}

async function main (){
    try {

        //get Employee from Source
        var sourceFieldsArray = [process.env.SOURCE_FULLNAME, process.env.SOURCE_FIRSTNAME, process.env.SOURCE_LASTNAME, process.env.SOURCE_EMAIL, process.env.SOURCE_STATUS]
        var sourceEmployeeList = await airtableAPI.getEmployee(process.env.SOURCE_API_KEY, process.env.SOURCE_BASE_ID, process.env.SOURCE_TABLE_ID, sourceFieldsArray)
        //writeToFile(sourceEmployeeList, "sourceEmployeeList.json")
        console.log(sourceEmployeeList.length + "-S");
        sleep(500)

        //get Employee from Destination
        var destinationFieldsArray = [process.env.DESTINATION_FULLNAME, process.env.DESTINATION_FIRSTNAME, process.env.DESTINATION_LASTNAME, process.env.DESTINATION_EMAIL, process.env.DESTINATION_STATUS]
        var destinationEmployeeList = await airtableAPI.getEmployee(process.env.DESTINATION_API_KEY, process.env.DESTINATION_BASE_ID, process.env.DESTINATION_TABLE_ID, destinationFieldsArray)
        //writeToFile(destinationEmployeeList, "destinationEmployeeList.json")
        console.log(destinationEmployeeList.length + "-D");
        sleep(500)

        for (let s = 0; s < sourceEmployeeList.length; s++) {
            console.log(s);
            var eleSourceEmployeeList = sourceEmployeeList[s];
            var found = false
            var recId="";

            for (let d = 0; d < destinationEmployeeList.length; d++) {
                var eleDestinationEmployeeList = destinationEmployeeList[d];

                //Company Mail Id Match
                if (eleDestinationEmployeeList[sourceFieldsArray[3]] != null && eleSourceEmployeeList[sourceFieldsArray[3]] != null && eleSourceEmployeeList[sourceFieldsArray[3]].includes("@")) {
                    if (eleDestinationEmployeeList[sourceFieldsArray[3]].replace(/\s/g, "") == eleSourceEmployeeList[sourceFieldsArray[3]].replace(/\s/g, "")) {
                        // console.log(eleDestinationEmployeeList[sourceFieldsArray[9]],eleSourceEmployeeList[sourceFieldsArray[9]]);
                        //Company Mail Id Match True
                        found = true;
                        recId=eleDestinationEmployeeList["recId"]
                        break;
                    }
                } else {
                    //Full Name Match
                    if (eleDestinationEmployeeList[sourceFieldsArray[0]] != null && eleSourceEmployeeList[sourceFieldsArray[0]] != null) {
                        if (eleDestinationEmployeeList[sourceFieldsArray[0]].replace(/\s/g, "") == eleSourceEmployeeList[sourceFieldsArray[0]].replace(/\s/g, "")) {
                            // console.log(eleDestinationEmployeeList[sourceFieldsArray[0]],eleSourceEmployeeList[sourceFieldsArray[0]]);
                            //Full Name Match True
                            found = true;
                            recId=eleDestinationEmployeeList["recId"]
                            break;
                        }
                    }

                }

                

            }

            var destinationObj = {}

            destinationObj[destinationFieldsArray[0]] = eleSourceEmployeeList[sourceFieldsArray[0]]
            destinationObj[destinationFieldsArray[1]] = eleSourceEmployeeList[sourceFieldsArray[1]]
            destinationObj[destinationFieldsArray[2]] = eleSourceEmployeeList[sourceFieldsArray[2]]
            destinationObj[destinationFieldsArray[3]] = eleSourceEmployeeList[sourceFieldsArray[3]]
            destinationObj[destinationFieldsArray[4]] = eleSourceEmployeeList[sourceFieldsArray[4]]
            

            if (found == true) {
                console.log("Updating " + destinationObj[destinationFieldsArray[0]]);
                try {
                    var status = await airtableAPI.updateEmployee(process.env.DESTINATION_API_KEY, process.env.DESTINATION_BASE_ID, process.env.DESTINATION_TABLE_ID,recId, destinationObj)
                    sleep(500)
                } catch (error) {
                    console.log("U " + error);
                }
                if (status == "Success") {
                    status=null
                    console.log("Updated " + destinationObj[destinationFieldsArray[0]])

                    //get Employee from Destination
                    var destinationEmployeeList = await airtableAPI.getEmployee(process.env.DESTINATION_API_KEY, process.env.DESTINATION_BASE_ID, process.env.DESTINATION_TABLE_ID, destinationFieldsArray)
                    //writeToFile(destinationEmployeeList, "destinationEmployeeList.json")
                    console.log(destinationEmployeeList.length + "-D");
                    sleep(500)

                }
            
            } else {
                console.log("Creating " + destinationObj[destinationFieldsArray[0]]);
                try {
                    var status = await airtableAPI.createEmployee(process.env.DESTINATION_API_KEY, process.env.DESTINATION_BASE_ID, process.env.DESTINATION_TABLE_ID, destinationObj)
                    sleep(500)
                } catch (error) {
                    console.log("C " + error);
                }
                if (status == "Success") {
                    status=null
                    console.log("Created " + destinationObj[destinationFieldsArray[0]])

                    //get Employee from Destination
                    var destinationEmployeeList = await airtableAPI.getEmployee(process.env.DESTINATION_API_KEY, process.env.DESTINATION_BASE_ID, process.env.DESTINATION_TABLE_ID, destinationFieldsArray)
                    //writeToFile(destinationEmployeeList, "destinationEmployeeList.json")
                    console.log(destinationEmployeeList.length + "-D");
                    sleep(500)

                }
            }

        }
    } catch (error) {
        console.log("catch");
        console.log(error);
    }
}


main();
