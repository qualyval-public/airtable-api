var airtable = require('airtable');

//GetEmployee
function getEmployee(airtableAPIKey, baseId, tableId, fieldsArray) {
    return new Promise((resolve, reject) => {
        try {
            var employee = [];

            let base = new airtable({ apiKey: airtableAPIKey }).base(baseId);
            base(tableId).select({ fields: fieldsArray })
                .eachPage((records, fetchNextPage) => {
                    records.forEach(item => {
                        let obj = {};

                        obj["recId"] = item.id;

                        //Full Name
                        if (item.fields[fieldsArray[0]] == undefined) {
                            obj[fieldsArray[0]] = null
                        }
                        else {
                            obj[fieldsArray[0]] = item.fields[fieldsArray[0]]
                        }

                        //First Name
                        if (item.fields[fieldsArray[1]] == undefined) {
                            obj[fieldsArray[1]] = null
                        }
                        else {
                            obj[fieldsArray[1]] = item.fields[fieldsArray[1]]
                        }

                        //Last Name
                        if (item.fields[fieldsArray[2]] == undefined) {
                            obj[fieldsArray[2]] = null
                        }
                        else {
                            obj[fieldsArray[2]] = item.fields[fieldsArray[2]]
                        }

                        //Status
                        if (item.fields[fieldsArray[3]] == undefined) {
                            obj[fieldsArray[3]] = null
                        }
                        else {
                            obj[fieldsArray[3]] = item.fields[fieldsArray[3]]
                        }

                        //Email
                        if (item.fields[fieldsArray[4]] == undefined) {
                            obj[fieldsArray[4]] = null
                        }
                        else {
                            obj[fieldsArray[4]] = item.fields[fieldsArray[4]]
                        }

                        employee.push(obj)
                    });

                    fetchNextPage();
                }, function done(err) {
                    if (err) {
                        console.log(err);
                        reject('Erorr fetching employee list ' + err);
                    } else {
                        resolve(employee);
                    }
                });
        } catch (error) {
            console.log(error);
            reject('Erorr fetching employee list ' + error);
        }


    });
}

//CreateEmployee
function createEmployee(airtableAPIKey, baseId, tableId, obj) {
    return new Promise((resolve, reject) => {
        try {

            let base = new airtable({ apiKey: airtableAPIKey }).base(baseId);
            base(tableId).create(obj,{typecast: true},
                function (err, record) {
                    if (err) {
                        console.error(err);
                        reject('Erorr creating employee c ' + err);
                    }
                    resolve("Success");
                })

        } catch (error) {
            console.log(error);
            reject('Erorr creating employee ' + error);
        }


    });
}

//UpdateEmployee
function updateEmployee(airtableAPIKey, baseId, tableId,recId, obj) {
    return new Promise((resolve, reject) => {
        try {

            let base = new airtable({ apiKey: airtableAPIKey }).base(baseId);
            base(tableId).update(recId,obj,{typecast: true},
                function (err, record) {
                    if (err) {
                        console.error(err);
                        reject('Erorr updating employee c ' + err);
                    }
                    resolve("Success");
                })

        } catch (error) {
            console.log(error);
            reject('Erorr updating employee ' + error);
        }


    });
}

module.exports.getEmployee = getEmployee;
module.exports.createEmployee = createEmployee;
module.exports.updateEmployee = updateEmployee;
